from itertools import combinations
from math import sqrt

from mrjob.job import MRJob
from mrjob.step import MRStep


class MovieRecommender(MRJob):

    def configure_args(self):
        super(MovieRecommender, self).configure_args()
        # path to the input data
        # goes to every node
        self.add_file_arg('--items')

    # define map&reduce orders
    def steps(self):
        return [
            MRStep(mapper=self.extractUserRatingsMap,
                   reducer=self.groupUserRatingsReduce),
            MRStep(mapper=self.groupByUserMap,
                   reducer=self.computeSimilarityReduce),
            MRStep(mapper=self.arrangeFromSimilarityMap,
                   mapper_init=self.getMovieNames,
                   reducer=self.groupSimilarMoviesReduce)
        ]

    # for each user, get the user id and the (movie, it's rating)
    def extractUserRatingsMap(self, _, line):
        userID, movieID, rawRating, _ = line.split(",")
        try:
            yield userID, (movieID, float(rawRating))
        except:
            pass

    # get all the movies rated by the same user (so have user: (movie1, rating1), (movie2, rating2) etc.)
    def groupUserRatingsReduce(self, userID, movieRatings):
        ratings = []
        for movie, rating in movieRatings:
            ratings.append((movie, rating))
        yield userID, ratings

    # receives all the movies rated by the same user and combines them, two by two (cartesian product)
    def groupByUserMap(self, userID, allMovieRatings):
        for firstMovieRating, secondMovieRating in combinations(allMovieRatings, 2):
            yield (firstMovieRating[0], secondMovieRating[0]), (firstMovieRating[1], secondMovieRating[1])
            yield (secondMovieRating[0], firstMovieRating[0]), (secondMovieRating[1], firstMovieRating[1])

    # receives pairs of movies (m1, m2) with their ratings (from more users) -> [(r1, r2), (r3, r4) ...]
    # computes cosine similarity of their ratings and if better than threshold => movies similar to each other
    def computeSimilarityReduce(self, moviePair, ratingPairs):
        score, pairsCount = self.getSimilarity(ratingPairs)
        # the number of rating should be over a considered relevant number of ratings, right now 10
        # the score should be over the 0.95 treshold to be considered similar enough
        if pairsCount > 10 and score > 0.95:
            yield moviePair, (score, pairsCount)

    # Uses cosine similarity: a . b / sqrt(a^2)) .sqrt((b^2))
    def getSimilarity(self, ratingPairs):
        sumAB = 0
        sumAA = 0
        sumBB = 0
        pairsCount = 0
        score = 0

        for a, b in ratingPairs:
            sumAB += a * b
            sumAA += a * a
            sumBB += b * b
            pairsCount += 1

        numerator = sumAB
        denominator = sqrt(sumAA) * sqrt(sumBB)

        if denominator != 0:
            score = numerator / denominator

        return score, pairsCount

    # receives the movie pair and transforms it by recommending the second movie for the first
    # one, with its score and total ratings
    def arrangeFromSimilarityMap(self, moviePair, scoreRatingsCount):
        try:
            firstMovie, secondMovie = moviePair
            score, ratings = scoreRatingsCount

            yield self.movieNames[int(firstMovie)], self.movieNames[int(secondMovie)] + '{' + str(
                score) + '}' + '{' + str(ratings) + '}'
        except:
            pass

    # keep all the movies recommended to a certain movie in one place
    def groupSimilarMoviesReduce(self, movie, similarMovies):
        allSimilarMovies = []
        try:
            for movie in similarMovies:
                allSimilarMovies.append(movie)

            yield movie, allSimilarMovies
        except:
            pass

    # create a dictionary with movieId -> movieName
    def getMovieNames(self):
        self.movieNames = {}
        with open('ml-20m/movies.csv', encoding="utf8") as f:
            for line in f:
                fields = line.split(',')
                try:
                    self.movieNames[int(fields[0])] = fields[1]
                except:
                    pass


if __name__ == '__main__':
    MovieRecommender.run()
