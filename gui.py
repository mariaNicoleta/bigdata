from tkinter import *


class Application(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)

        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.allLabel = Label(self, textvariable=StringVar(value="Choose some movies you like:"))
        self.allLabel.grid(row=0, column=0, padx=3, pady=3)

        self.likedLabel = Label(self, textvariable=StringVar(value="So you'd like to see something like:"))
        self.likedLabel.grid(row=0, column=2, padx=3, pady=3)

        self.recommendedLabel = Label(self, textvariable=StringVar(value="Your recommendations:"))
        self.recommendedLabel.grid(row=0, column=4, padx=3, pady=3)

        self.searchBox = StringVar()
        self.searchBox.trace("w", lambda name, index, mode: self.updateMoviesList())
        self.entry = Entry(self, textvariable=self.searchBox, width=45)
        self.entry.grid(row=1, column=0, padx=3, pady=3)

        self.listBox = Listbox(self, width=45, height=25)
        self.listBox.grid(row=2, column=0, padx=3, pady=3)

        self.updateMoviesList()

        self.copyButton = Button(self, text="--->", command=self.addMovieToLiked, width=10)
        self.copyButton.grid(row=2, column=1, padx=3, pady=3)

        self.likedMoviesList = Listbox(self, width=45, height=25)
        self.likedMoviesList.grid(row=2, column=2, padx=3, pady=3)

        self.computeButton = Button(self, text="Recommend me!", command=self.recommendMovies, width=15)
        self.computeButton.grid(row=2, column=3, padx=3, pady=3)

        self.recommendedMoviesList = Listbox(self, width=45, height=25)
        self.recommendedMoviesList.grid(row=2, column=4, padx=3, pady=3)

    def addMovieToLiked(self):
        selected = self.listBox.curselection()

        if selected:
            for item in selected:
                value = self.listBox.get(item)
                self.likedMoviesList.insert(END, value + "\n")

    def recommendMovies(self):
        print("Loading...")

    def updateMoviesList(self):
        search_term = self.searchBox.get()

        lbox_list = ['Adam', 'Lucy', 'Barry', 'Bob',
                     'James', 'Frank', 'Susan', 'Amanda', 'Christie']

        self.listBox.delete(0, END)

        for item in lbox_list:
            if search_term.lower() in item.lower():
                self.listBox.insert(END, item)


root = Tk()
root.title('Filter Listbox Test')
app = Application(master=root)
app.mainloop()
